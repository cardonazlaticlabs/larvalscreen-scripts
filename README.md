# Larvalscreen file tracking

## src

Source data.
Commands are to be executed from the LarvalScreen root.

### src/manifest.txt

Manifest of all non-hidden files.
Does not include symlinks.
Generated with

```sh
sudo find . -not -path '*/\.*' -type f | cut -c 3- | sort > $THIS_DIR/src/manifest.txt
```

### src/duplicates.txt

Byte-for-byte duplicated files.
Generated with

```sh
sudo $(which fddf) . > $THIS_DIR/src/duplicates.txt
```

Requires [fddf](https://github.com/birkenfeld/fddf) (`cargo install fddf`).

### src/broken_links.txt

Finds the absolute path of any dangling (i.e. broken) relative or absolute symlink, and its (nonexistent) target, as a tab-separated value file.

```sh
sudo symlinks -rv . | grep '^dangling' | cut -c 11- | sed 's/ -> /\t/' > ~/broken_links.tsv
```

Get only the first fields with `cut -f 1 ~/broken_links.tsv` (`-f 2` for the second field).

## tgt

Data derived from the source data.
Generated in this repo by the Makefile (`make`).

### tgt/link_matches.txt

`make tgt/link_matches.txt`

For every broken symlink, find any files with the same name stem
(e.g. `A/B/myfile.txt` and `X/Y/myfile.csv` match).
For each:

- count how many of its ancestor directories have the same name as the query symlink's
- count how many of its extensions are shared (e.g. `myfile.lsm.bz2` and `myfile.lsm` have 1)

### tgt/extensions.txt

`make tgt/extensions.txt`

Digest of terminal file extensions in the manifest.

### tgt/dirs.txt

`make tgt/dirs.txt`

Digest of directories in the manifest, with the number of direct child files.

### tgt/searches/*

`make tgt-searches`

Try to find some lines using a very coarse regex.

### tgt/lsm_problems.txt

`make tgt/lsm_problems.txt`

List of LSM files where there is neither a TIFF or compressed LSM with the same filename stem.
Uses case-insensitive searches for `.lsm`, `.tif`, `.tiff`, and `.lsm.bz2`.

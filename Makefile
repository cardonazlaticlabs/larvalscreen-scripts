default: tgt/link_matches.txt tgt/extensions.txt tgt/dirs.txt tgt/lsm_problems.txt tgt-searches

tgt:
	mkdir -p tgt

tgt/dirs.txt: tgt
	rev src/manifest.txt | cut -d / -f 2- | rev | sort | uniq -c > tgt/dirs.txt

tgt/extensions.txt: tgt
	rev src/manifest.txt | cut -d . -f 1 | rev | sort | uniq -c | sort -nr > tgt/extensions.txt

tgt/link_matches.txt: tgt
	python3 scripts/dangling_best_match.py src/manifest.txt src/broken_links.tsv > tgt/link_matches.txt

tgt/lsm_problems.txt: tgt/link_matches.txt
	python3 scripts/lsm_problems.py tgt/link_matches.txt > tgt/lsm_problems.txt

.PHONY: tgt-searches
tgt-searches: tgt
	python3 scripts/searches.py src/manifest.txt tgt/searches

.PHONY: clean
clean:
	rm -rf tgt

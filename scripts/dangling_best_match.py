#!/usr/bin/env python
from pathlib import Path
import csv
import os
from collections import defaultdict
import logging
from argparse import ArgumentParser

logger = logging.getLogger(__name__)


def get_stem(p: Path):
    return p.name.split(".")[0]



def shared_ancestors(p1: Path, p2: Path) -> int:
    count = 0
    root = Path("/")
    for par1, par2 in zip(p1.parents, p2.parents):
        if par1.name == par2.name:
            count += 1
        else:
            break
        if par1 == root or par2 == root:
            break
    return count


def main():
    logging.basicConfig(level=logging.DEBUG)
    parser = ArgumentParser()
    parser.add_argument("manifest", help="newline-separated manifest file")
    parser.add_argument("dangling", help="TSV file of broken link names and their targets")
    parsed = parser.parse_args()

    with open(parsed.manifest) as f:
        manifest_by_stem = defaultdict(list)
        for l in f:
            p = Path(l.strip())
            manifest_by_stem[get_stem(p)].append(p)


    with open(parsed.dangling) as f:
        link_names = [Path(name) for name, _ in csv.reader(f, delimiter="\t")]

    first = True
    for name in link_names:
        link_stem = get_stem(name)

        matches = manifest_by_stem.get(link_stem, [])
        if matches:
            if first:
                first = False
            else:
                print("")
            print(os.fspath(name))
            for m in matches:
                shared_ext = len(set(m.suffixes).intersection(name.suffixes))
                n_shared_anc = shared_ancestors(name, m)
                print(f"\tshared_ancestors:{n_shared_anc}\tshared_ext:{shared_ext}\t{m}")
        else:
            logger.warning("No file stem matches for %s", name)


if __name__ == "__main__":
    main()

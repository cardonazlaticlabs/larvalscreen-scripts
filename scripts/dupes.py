from dataclasses import dataclass
from typing import Set
import os
from pathlib import Path


@dataclass
class Duplicates:
    n_bytes: int
    paths: Set[Path]

    def to_str(self):
        joiner = "\n" + " " * 4
        paths = [os.fspath(p) for p in sorted(self.paths)]
        return joiner.join([f"Size {self.n_bytes} bytes:"] + paths)

    def __len__(self):
        return len(self.paths)

    @classmethod
    def from_str(cls, s):
        lines = s.strip().split("\n")
        n_bytes = int(lines.pop(0).strip()[5:-7])
        paths = {Path(p.strip()) for p in lines}
        return cls(n_bytes, paths)


with open("larvalscreen/duplicates.txt") as f:
    s = f.read()

dupes = [Duplicates.from_str(block) for block in s.split("\n\n")]

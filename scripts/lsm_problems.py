#!/usr/bin/env python
"""
Report any LSM files represented by broken links which do not have either a .tif or .lsm.bz2 with the same name stem.
"""
import argparse
from dataclasses import dataclass
import logging
from pathlib import Path
import os

logger = logging.getLogger(__name__)


@dataclass
class IsoStem:
    shared_ancestors: int
    shared_ext: int
    path: Path

    def to_str(self):
        print(f"shared_ancestors:{self.shared_ancestors}\tshared_ext:{self.shared_ext}\t{os.fspath(self.path)}")

    @classmethod
    def from_str(cls, s):
        items = s.strip().split("\t")
        p = Path(items.pop())
        if not items:
            logger.warning("NO items for %s\n%s", p, s)
        d = dict(i.split(":") for i in items)
        return cls(int(d["shared_ancestors"]), int(d["shared_ext"]), p)


def is_tif(path: Path):
    suff = path.suffix.lower()
    return suff in (".tif", ".tiff")


def is_lsm_bz2(path: Path):
    return path.name.lower().endswith(".lsm.bz2")


def process_block(block: str):
    lines = block.split("\n")
    original = Path(lines.pop(0).strip())
    if original.suffix.lower() != ".lsm":
        return
    if not lines:
        print("No same-stem files for " + os.fspath(original))

    has_tif = 0
    has_bz2 = 0
    for line in lines:
        iso = IsoStem.from_str(line)
        has_tif += is_tif(iso.path)
        has_bz2 += is_lsm_bz2(iso.path)

    if not has_bz2 and not has_tif:
        print("No TIFF or compressed LSM file for " + os.fspath(original))


def main():
    logging.basicConfig(level=logging.INFO)
    parser = argparse.ArgumentParser()
    parser.add_argument("linkmatches")
    parsed = parser.parse_args()
    with open(parsed.linkmatches) as f:
        s = f.read()

    for block in s.split("\n\n"):
        stripped = block.strip()
        if not stripped:
            logger.warning("Skipped empty block")
            continue

        process_block(stripped)


if __name__ == "__main__":
    main()

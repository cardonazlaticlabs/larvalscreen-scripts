#!/usr/bin/env python
from contextlib import contextmanager
import logging
from pathlib import Path
import re
from typing import Iterator
import zipfile
from argparse import ArgumentParser

logger = logging.getLogger(__name__)


def yield_lines(f) -> Iterator[bytes]:
    for line in f:
        yield line.strip(b"\n")


class Processor:
    def __init__(self, manifest_path, outdir_path) -> None:
        self.manifest_path = Path(manifest_path)
        self.outdir_path = Path(outdir_path)
        self.outdir_path.mkdir(parents=True, exist_ok=True)

    @contextmanager
    def manifest_lines(self):
        with open(self.manifest_path, "rb") as f:
            yield yield_lines(f)

    def _process(self, fname, *regexes: bytes):
        """Line must match ALL regexes"""
        res = [re.compile(r) for r in regexes]
        count_all = 0
        count_match = 0
        with open(self.outdir_path / f"{fname}.txt", "wb") as f, self.manifest_lines() as lines:
            for line in lines:
                if all(r.search(line) for r in res):
                    f.write(line + b"\n")
                    count_match += 1
                count_all += 1
        logger.info("%s: wrote %s of %s", fname, count_match, count_all)

    def split_gal4(self):
        """# 1) all screen images of larval split GAL4 lines
        name of either file or one of the directories the files is in should contain SS0... or MB....and 40X

        INTERPRETATION: "(SS0... or MB...) and (40X)"
        """
        self._process("1_split_gal4", rb"(SS0|MB)", rb"40X")

    def high_res_split_gal4(self):
        """2) all high-res images of larval split GAL4 lines that were taken
        look for ....SS0 or MB... and 63x

        INTERPRETATION: "(...SS0 or MB...) and (63x)
        """
        self._process("2_high_res_split_gal4", rb"(SS0|MB)", rb"40X")

    def mcfo_split_gal4(self):
        """3) all MCFO images of larval Split GAL4 lines
        look for ...MCFO and SS0.. or MB..

        INTERPRETATION: "(...MCFO) and (SS0.. or MB..)"
        """
        self._process("3_mcfo_split_gal4", rb"MCFO", rb"(SS0|MB)")

    def mcfo_gal4(self):
        """4) all MCFO images of larval GAL4 lines
        look for...MCFO and GMR... or R.... or VT....

        INTERPRETATION:
        """
        self._process("4_mcfo_gal4", rb"MCFO", rb"(GMR|R|VT)")

    def gmr_gal4(self):
        """5) all larval images of GMR GAL4 lines
        name should contain GMR or R...or GAL4
        """
        self._process("5a_gmr_gal4", rb"(GMR|R|GAL4)")

    def vienna_gal4(self):
        """5) all larval images of Vienna GAL4 lines
        name should contain VT... or GAL4
        """
        self._process("5b_vienna_gal4", rb"(VT|GAL4)")

    def gmr_lexA(self):
        """6) all larval images of GMR LexA lines
        name should contain GMR or R and LexA or both
        """
        raise NotImplementedError()
        self._process("6_gmr_lxA", rb"")

    def vienna_lexA(self):
        """7) all larval images of Vienna LexA lines
        name should contain VT or LexA or both
        """
        self._process("7_vienna_lexA", rb"(VT|LexA)")


def main():
    logging.basicConfig(level=logging.DEBUG)
    parser = ArgumentParser()
    parser.add_argument("manifest", help="newline-separated manifest file")
    parser.add_argument("outdir", help="directory to save outputs in")
    parsed = parser.parse_args()

    proc = Processor(parsed.manifest, parsed.outdir)

    for fn in [
        proc.split_gal4,
        proc.high_res_split_gal4,
        proc.mcfo_split_gal4,
        proc.mcfo_gal4,
        proc.gmr_gal4,
        proc.vienna_gal4,
        proc.gmr_lexA,
        proc.vienna_lexA,
    ]:
        try:
            fn()
        except NotImplementedError:
            logger.warning("Not implemented: %s", fn)


if __name__ == "__main__":
    main()
